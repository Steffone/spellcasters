﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuccessfullSpellParticle : MonoBehaviour
{
    [SerializeField] SpriteRenderer _spellSpriteRenderer;
    public void SetSprite(Sprite icon)
    {
        _spellSpriteRenderer.sprite = icon;
    }
}
