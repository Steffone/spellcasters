﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[ExecuteAlways]
public class UILineRenderer : MonoBehaviour
{
    public Sprite ImageSprite;
    public List<Vector2> Points;
    public float Width;
    public Color Color;
    public Material Material;
    private List<Image> _images;
    private float _widthFactor;
    private float _heightFactor;
    private void Start()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
         _widthFactor = (float)rectTransform.rect.width/(float)Screen.width ;
         _heightFactor = (float)rectTransform.rect.height/(float)Screen.height;
    }

    private void Update()
    {
        if (_images == null)
        {
            _images = new List<Image>();
        }
        if (_images.Count < Points.Count - 1)
        {
            GameObject gameObj = new GameObject();
            gameObj.transform.parent = transform;
            gameObj.transform.localScale = Vector3.one;
            gameObj.transform.localPosition = Vector3.zero;
            Image img= gameObj.AddComponent<Image>();
            img.sprite = ImageSprite;
            img.rectTransform.pivot = new Vector2(0, 0.5f);
            img.rectTransform.anchorMax = Vector2.zero;
            img.rectTransform.anchorMin = Vector2.zero;
            img.color = Color;
            img.material = Material;
            img.type = Image.Type.Tiled;
            _images.Add(img);
        }
        if (_images.Count >= Points.Count - 1)
        {
            int start = Points.Count > 0 ? Points.Count - 1 : 0;
            for (int i = start; i < _images.Count; i++)
            {
                DestroyImmediate(_images[i].gameObject);
            }
            _images.RemoveRange(start, _images.Count - start);
        }
        for(int i = 0; i < _images.Count; i++)
        {
            Vector3 pos = Points[i];
            pos.z = transform.position.z;
            _images[i].transform.position = pos;
            Vector2 pos1 = new Vector2(Points[i + 1].x * _widthFactor, Points[i+1].y * _heightFactor);
            Vector2 pos2 = new Vector2(Points[i].x * _widthFactor, Points[i].y * _heightFactor);
            pos1 = UICamera.Instance.Cam.WorldToScreenPoint(pos1);
            pos2 = UICamera.Instance.Cam.WorldToScreenPoint(pos2);
            float distance = (pos1 - pos2).magnitude;
            _images[i].transform.right = (Points[i+1] - Points[i]).normalized;
            _images[i].rectTransform.sizeDelta = new Vector2(distance, Width);
        }
    }
}
