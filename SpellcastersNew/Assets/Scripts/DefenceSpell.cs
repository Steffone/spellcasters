﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Untitled defence Spell", menuName = "Spells/DefenceSpell")]
public class DefenceSpell : Spell
{
    private void Awake()
    {
        type = SpellType.Defence;
    }

    public override void OnStartCasting(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " OnStartCasting Defence" + this.name);
        playerController.SetupSpellAnimation();
    }

    public override void OnCast(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " OnCast" + this.name);
        GameManager.Instance.StopSlowDown();
    }

    public override void OnInitializeSpell(PlayerController playerController)
    {
        base.OnInitializeSpell(playerController);
        Debug.Log("" + playerController.name + " OnInitializeSpell_Defence " +this.name);
        playerController.StartCastingSpell();
        GameManager.Instance.Stage_CastSpells_DefenceStart();
    }
}
