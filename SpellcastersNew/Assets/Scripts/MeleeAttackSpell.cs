﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Untitled attack Spell", menuName = "Spells/AttackSpells/Melee Attack Spell")]
public class MeleeAttackSpell : AttackSpell
{

    public CloseRangeController closeRangeControllerPrefab;
    CloseRangeController closeRangeController;
    private void Awake()
    {
        type = SpellType.Attack;
    }

    public override void OnStartCasting(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " OnStartCasting " + this.name);
        OnCast(playerController);
        playerController.WeaponController.WeaponAttackBehaviour.attackSpell = this;
        closeRangeController = Instantiate(closeRangeControllerPrefab);
        closeRangeController.Initialize(playerController);
        closeRangeController.MoveTo();
    }

    public override void FinishSpell(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " FinishSpell MeleeSpell" + this.name);
        playerController.mainCollider.enabled = true;
        GameManager.Instance.Stage_CastSpellsAttackEnd(0);
    }
}