﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenceHandler : MonoBehaviour, ISpellHit
{

    public void HitSpell(Vector3 hitPosition, GameObject hitSource, AttackSpell attackSpell)
    {
        SpellEffect spellEffect = Utility.FindComponentInHierarchyBottomUp<SpellEffect>(transform);
        if (spellEffect != null)
        {
            spellEffect.playerOwner.HitSpell(hitPosition, hitSource, attackSpell);
        }
    }
}
