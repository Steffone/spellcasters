﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CharacterStats : MonoBehaviour
{
    public delegate void OnValueChange(float newValue, bool isChanged);

    public event OnValueChange OnMaxHealthChange;
    public event OnValueChange OnCurrentHealthChange;

    public float MaxHealth
    {
        get
        {
            return _maxHealth;
        }
        private set
        {
            OnMaxHealthChange?.Invoke(value, _maxHealth != value);
            _maxHealth = value;
        }
    }
    public float CurrentHealth
    {
        get
        {
            return _currentHealth;
        }
        set
        {
            OnCurrentHealthChange?.Invoke(value, _currentHealth != value);
            _currentHealth = value;
        }
    }

    [SerializeField] float _maxHealth;
    [SerializeField] float _currentHealth;
}
