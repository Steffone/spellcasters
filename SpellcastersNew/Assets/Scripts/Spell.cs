﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpellType
{
    Attack,
    Defence,
    Enchant,
    Buff
}

public enum SpellElement
{
    Fire,
    Water,
    Wind,
    Earth,
    Shadow,
    Lightning,
    Metal,
    Light,
    Force
}

public class Spell : ScriptableObject
{
    public string name;
    public string spellCode;
    public SpellType type;
    public Sprite icon;
    public AnimationClip animationClip;
    public SpellEffect effect;
    public List<SpellEffect> secondaryEffects;
    public float spellCastDuration;

    public Element element;

    public virtual void OnStartCasting(PlayerController playerController)
    {
    }

    public virtual void OnCast(PlayerController playerController)
    {
    }

    public virtual void OnInitializeSpell(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + "OnInitializeSpell" + this.name);
    }

    public virtual void FinishSpell(PlayerController playerController)
    {
    }
}