﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class DamageCanvas : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI damageText;
    public void SetDamageValue(float value)
    {
        damageText.text = value.ToString("n0");
    }
}
