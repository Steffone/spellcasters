﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemEffect : MonoBehaviour, ITarget
{
    [SerializeField] FollowBezier followBezier1;
    [SerializeField] FollowBezier followBezier2;

    [SerializeField] GameObject golemPrefab;

    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        //yield return new WaitForSeconds(0.1f);
        if (target != null)
        {
            followBezier1.SetTarget(target);
            followBezier2.SetTarget(target);
        }
        followBezier1.OnFinish += delegate
        {
            GameObject golem = Instantiate(golemPrefab, followBezier1.transform.position, Quaternion.identity);
        };
        followBezier1.Init();
        followBezier2.Init();
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
