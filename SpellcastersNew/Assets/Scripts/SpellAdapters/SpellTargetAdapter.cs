﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellTargetAdapter : SpellAdapter
{
    public Vector3 offsetPosition;
    public override void Adapt(PlayerController playerOwner)
    {
        PlayerController enemy = GameManager.Instance.GetEnemy(playerOwner);

        GameObject target = new GameObject();
        target.transform.parent = transform;
        target.transform.position = enemy.transform.position;
        target.transform.position += offsetPosition;

        GetComponent<ITarget>().SetTarget(target.transform);
    }
}
