﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLocationAdapter : SpellAdapter
{
    public override void Adapt(PlayerController playerOwner)
    {
        transform.position = playerOwner.transform.position;
    }
}
