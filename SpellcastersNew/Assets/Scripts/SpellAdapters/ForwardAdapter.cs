﻿using UnityEngine;
using System.Collections.Generic;

public class ForwardAdapter : SpellAdapter
{
    public override void Adapt(PlayerController playerOwner)
    {
        PlayerController enemy=GameManager.Instance.GetEnemy(playerOwner);
        Vector3 direction = enemy.transform.position - playerOwner.transform.position;
        transform.forward = direction;
    }
}
