﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpellAdapter : MonoBehaviour
{
    protected void Offset()
    {
        PositionOffset[] offsets = GetComponents<PositionOffset>();
        foreach(var i in offsets)
        {
            i.Offset();
        }

    }

    public abstract void Adapt(PlayerController playerOwner);
}
