﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLocationAdapter : SpellAdapter
{
    public override void Adapt(PlayerController playerOwner)
    {
        PlayerController enemy = GameManager.Instance.GetEnemy(playerOwner);
        transform.position = enemy.transform.position;
    }
}
