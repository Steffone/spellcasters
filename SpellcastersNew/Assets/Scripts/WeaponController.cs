﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public AttackBehaviour WeaponAttackBehaviour;

    List<Effect> _weaponEffects;

    private void Start()
    {
        _weaponEffects = PersonalUtility.FindComponentsInHierarchyTopDown<Effect>(WeaponAttackBehaviour.transform);
    }

    private void AnimationCallback_ActivateAttackBehaviour()
    {
        WeaponAttackBehaviour.enabled = true;
    }

    public void AnimationCallback_DeactivateAttackBehaviour()
    {
        WeaponAttackBehaviour.enabled = false;
    }

    public void DisableWeaponEffects()
    {
        foreach(var i in _weaponEffects)
        {
            i.gameObject.SetActive(false);
        }
    }
    public void EnableWeaponEffects()
    {
        foreach (var i in _weaponEffects)
        {
            i.gameObject.SetActive(true);
        }
    }
}
