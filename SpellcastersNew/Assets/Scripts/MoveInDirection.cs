﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MoveInDirection : MonoBehaviour
{
    public Vector3 direction;
    public float speed;
    Rigidbody rb;

    public ParticleSystem particle;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        particle.Play();
    }

    private void Update()
    {
        rb.MovePosition(rb.position + transform.forward * speed * Time.deltaTime);
    }
}
