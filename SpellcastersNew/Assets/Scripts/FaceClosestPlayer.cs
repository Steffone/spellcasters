﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceClosestPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        float distance = (transform.position - players[0].transform.position).sqrMagnitude;
        PlayerController player = players[0];
        foreach(var i in players)
        {
            float tmpDistance= (transform.position - i.transform.position).sqrMagnitude;
            if (tmpDistance < distance)
            {
                player = i;
                distance = tmpDistance;
            }
        }

        transform.forward = (player.transform.position - transform.position).normalized;
    }
}
