﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hittable_Instantiate : Hittable
{
    public GameObject[] effectPrefabs;

    public float destroyTime;

    public override void OnHit(Vector3 hitPosition, GameObject hitSource)
    {
        Vector3 hitPoint = hitPosition;
        Vector3 hitNormal = (hitPosition - transform.position).normalized;

        Debug.DrawLine(hitSource.transform.position, transform.position, Color.red, 5);

        foreach (var effect in effectPrefabs)
        {
            var instance = Instantiate(effect, hitPosition , new Quaternion()) as GameObject;
            instance.transform.forward = hitNormal;
            Destroy(instance, destroyTime);
        }
    }
}
