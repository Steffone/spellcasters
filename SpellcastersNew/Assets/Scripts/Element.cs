﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Spellcasters/Element",fileName ="Unnamed Element")]
public class Element : ScriptableObject
{
    public SpellElement elementType;
    public Resistances resistancesToElements;
}
