﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(CharacterStats))]
public class HealthController : MonoBehaviour
{
    [SerializeField] HealthUIPanel _healthPanelPrefab;
    private HealthUIPanel _healthPanelInstance;
    [SerializeField] Transform _healthUIAncor;
    CharacterStats _characterStats;

    [SerializeField] List<HealthUIPanel> HealthUIPanels;
    
    private void Start()
    {
        _characterStats = GetComponent<CharacterStats>();
        _characterStats.CurrentHealth = _characterStats.MaxHealth;
        //_healthPanelInstance = Instantiate(_healthPanelPrefab);
        //_healthPanelInstance.Bind(_characterStats);
        foreach(var i in HealthUIPanels)
        {
            i.Bind(_characterStats);
        }
    }

    public bool ReduceHealth(float amount)
    {
        _characterStats.CurrentHealth -= amount;
        if (_characterStats.CurrentHealth <= 0)
        {
            _characterStats.CurrentHealth = 0;
        }
        return _characterStats.CurrentHealth == 0;
    }

    public void IncreaseHealth(float amount)
    {
        _characterStats.CurrentHealth += amount;
        if (_characterStats.CurrentHealth >= _characterStats.MaxHealth)
        {
            _characterStats.CurrentHealth = _characterStats.MaxHealth;
        }
    }

    public float GetHealth()
    {
        return _characterStats.CurrentHealth;
    }
}
