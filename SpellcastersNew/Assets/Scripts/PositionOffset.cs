﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionOffset : MonoBehaviour
{
    [SerializeField] Transform objectToOffset;
    [SerializeField] Vector3 position;

    public void Offset()
    {
        objectToOffset.position += objectToOffset.forward * position.z;
        objectToOffset.position += objectToOffset.right * position.x;
        objectToOffset.position += objectToOffset.up * position.y;
    }
}
