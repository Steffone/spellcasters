﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(RagdollController))]
public class RagdollControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Enable"))
        {
            RagdollController rc = (RagdollController)target;
            rc.EnableRagdoll();
        }
        if (GUILayout.Button("Disable"))
        {
            RagdollController rc = (RagdollController)target;
            rc.DisableRagdoll();
        }
    }
}
#endif

public class RagdollController : MonoBehaviour
{
    public List<GameObject> ragdollObjects;

    public void EnableRagdoll()
    {
        foreach(var i in ragdollObjects)
        {
            i.GetComponent<Rigidbody>().isKinematic = false;
            i.GetComponent<Collider>().enabled = true;
        }
    }

    public void DisableRagdoll()
    {
        foreach (var i in ragdollObjects)
        {
            i.GetComponent<Rigidbody>().isKinematic = true;
            i.GetComponent<Collider>().enabled = false;
        }
    }
}
