﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class SpellCodeCreatorEditor : EditorWindow
{
    Spell spell;
    int rows;
    int cols;

    bool createGridFlag=false;

    int[] gridButtons;

    int testInt;

    [MenuItem("Spellcasters/Spell code creator")]
    static void Init()
    {
        SpellCodeCreatorEditor window = (SpellCodeCreatorEditor)EditorWindow.GetWindow(typeof(SpellCodeCreatorEditor));
        window.Show();
    }

    private void OnGUI()
    {

        spell = (Spell)EditorGUILayout.ObjectField("Spell",spell, typeof(Spell),true);
        rows = EditorGUILayout.IntField("Rows",rows);
        cols = EditorGUILayout.IntField("Cols", cols);

        if(GUILayout.Button("Create Grid"))
        {
            createGridFlag = true;
        }

        if (createGridFlag)
        {
            if (gridButtons == null)
            {
                gridButtons = new int[rows * cols];
            }
            int count = 1;
            EditorGUILayout.BeginVertical();
            for (int i = 0; i < rows; i++)
            {
                EditorGUILayout.BeginHorizontal();
                for (int j = 0; j < cols; j++)
                {
                    gridButtons[count - 1]=EditorGUILayout.IntField(gridButtons[count - 1], 
                        GUILayout.MinWidth(50), GUILayout.MinHeight(50),
                        GUILayout.MaxHeight(50),GUILayout.MaxWidth(50));
                    count++;
                }
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndVertical();


            if (GUILayout.Button("Create Code"))
            {
                if (spell == null)
                {
                    EditorUtility.DisplayDialog("Error", "Spell field is empty", "OK");
                    return;
                }
                string spellCode = "";
                for (int i = 1; i < gridButtons.Length + 1; i++)
                {
                    int checkCount = 0;
                    int index = -1;
                    for (int j = 0; j < gridButtons.Length; j++)
                    {
                        if (gridButtons[j] == i)
                        {
                            checkCount++;
                            index = j;
                        }
                    }
                    if (checkCount > 1)
                    {
                        EditorUtility.DisplayDialog("Error", "Two field have the same number", "OK");
                        return;
                    }
                    if (checkCount == 1)
                    {
                        spellCode += "|" + (index+1);
                    }
                }
                SpellDatabase spellDatabase = FindObjectOfType<SpellDatabase>();
                if (spellDatabase.Exists(spellCode))
                {
                    EditorUtility.DisplayDialog("Error", "Spell with code: " + spellCode+" already exists", "OK");
                    return;
                }
                EditorUtility.SetDirty(spell);
                spell.spellCode = spellCode;
                EditorUtility.DisplayDialog("Success", "Spell code: "+spellCode, "OK");
                AssetDatabase.SaveAssets();
            }
        }

    }
}
