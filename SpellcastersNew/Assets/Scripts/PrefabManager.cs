﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    [System.Serializable]
    public struct PrefabData
    {
        public string name;
        public GameObject prefab;
    }

    public static PrefabManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    [SerializeField] List<PrefabData> prefabCollection;

    public T GetPrefabInstance<T>(string name)
    {
        GameObject prefab = prefabCollection.Find(x => x.name == name).prefab;
        if (prefab == null)
        {
            Debug.LogError("The prefab with name " + name + " doesnt exist!");
            return default(T);
        }
        GameObject instance = Instantiate(prefab);
        T comp = instance.GetComponent<T>();
        if (comp == null)
        {
            Debug.LogError("The prefab with name " + name + " doesnt have the component "+ typeof(T).ToString());
            return default(T);
        }
        return comp;
    }

}
