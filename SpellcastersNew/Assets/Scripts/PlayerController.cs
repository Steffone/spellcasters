﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, ISpellHit,IHittable,IHitEffectApply
{
    public List<Spell> spells;
    public Collider mainCollider { get; private set; }
    public HealthController healthController { get; private set; }
    public WeaponController WeaponController;

    [SerializeField] Animator animator;
    [SerializeField] AnimatorOverrideController overrideController;
    Spell currentSpell;
    SpellEffect spellEffect;
    int secondaryEffectCount = 0;
    [SerializeField] GameObject activeIndicator;
    [SerializeField] ParticleSystem successfulSpellIndicator;
    [SerializeField] ParticleSystem chargeEffect;
    RagdollController ragdollController;

    private void Start()
    {
        healthController = GetComponent<HealthController>();
        mainCollider = GetComponent<Collider>();
        ragdollController = GetComponent<RagdollController>();
        ragdollController.DisableRagdoll();
        //spells = new List<Spell>();
        overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        animator.runtimeAnimatorController = overrideController;
    }

    #region Spell Cast
    public virtual void OpenSpellCastMenu()
    {
        ResetAnimatorSpeed();
        ResetSpellEffect();
        if (!HasAttackSpells())
        {
            GameManager.Instance.Stage_CastSpellsAttackEnd(0);
            return;
        }
        SpellCastMenuController.Instance.Open(this);
    }

    public virtual void CloseSpellCastMenu()
    {
        SpellCastMenuController.Instance.Close();
    }

    public virtual void OpenSpellCastMenuDefence()
    {
        Debug.Log("" + this.name + "OpenSpellCastMenuDefence");
        SpellCastMenuController.Instance.OpenDefence(this);
    }
    #endregion

    #region Spell Grid
    public virtual void OpenSpellGrid()
    {
        SpellGridController.Instance.Open(this);
    }

    public virtual void CloseSpellGrid()
    {
        SpellGridController.Instance.Close();
    }

    //private string CreateSpellCode()
    //{
    //    List<SpellGridPoint> spellGridPoints = SpellGridController.Instance.spellGridPoints;
    //    string spellCode = "";

    //    foreach (var i in spellGridPoints)
    //    {
    //        spellCode += "|" + i.id;
    //    }

    //    return spellCode;
    //}

    //public void OnSpellCreateComplete()
    //{
    //    string spellCode = CreateSpellCode();
    //    Spell spell = SpellDatabase.Instance.GetSpell(spellCode);
    //    if (spell != null)
    //    {
    //        SuccessfulCreatedSpell();
    //        spells.Add(spell);
    //    }
    //}

    public virtual void AddSpell(Spell spell)
    {
        SuccessfulCreatedSpell();
        spells.Add(spell);
    }

    public virtual void SuccessfulCreatedSpell()
    {
        successfulSpellIndicator.Play();
    }

    #endregion

    public virtual void RemoveSpell(Spell spell)
    {
        spells.Remove(spell);
    }

    public virtual void Activate()
    {
        ResetAnimatorSpeed();
        activeIndicator.SetActive(true);
    }
    
    public virtual void DeactivateIndicator()
    {
        activeIndicator.SetActive(false);
    }

    public virtual void StartCastSpell(Spell spell)
    {
        Debug.Log("" + this.name + " StartCastSpell " + spell.name);
        RemoveSpell(spell);

        mainCollider.enabled = false;

        currentSpell = spell;
        currentSpell.OnInitializeSpell(this);
    }

    public virtual void PlayChargeEffect()
    {
        ParticleSystem.MainModule main = chargeEffect.main;
        main.startLifetime = GameManager.Instance.gameSettings.STAGE_CastSpells_AttakChargeTime;
        chargeEffect.Play();
    }

    public virtual void StartCastingSpell()
    {
        Debug.Log("" + this.name + " StartCastingSpell " +currentSpell.name);
        currentSpell.OnStartCasting(this);
    }

    public virtual void MovingAnimation(bool isMoving)
    {
        animator.SetBool("Moving", isMoving);
    }
    public virtual void SetMovementSpeedAnimation(float speed)
    {
        animator.SetFloat("MovementSpeed", speed);
    }

    public virtual void PlayMovementClip(AnimationClip clip)
    {
        overrideController["EmptyMove"] = clip;
        animator.SetTrigger("Move");
    }
    
    //public void MeleeSpellCast()
    //{
    //    currentSpell.OnCast(this);
    //    teleportController.StartMeleeAttack();
    //}

    public virtual void SetupSpellAnimation()
    {
        Debug.Log("" + this.name + " SetupSpellAnimation" + currentSpell.name);
        overrideController["Empty"] = currentSpell.animationClip;
        animator.SetTrigger("Cast");
    }

    //Animation Call
    public virtual void ReadySpell()
    {
        Debug.Log("" + this.name + " ReadySpell" +currentSpell.name);
        if (currentSpell.secondaryEffects.Count > secondaryEffectCount)
        {
            spellEffect = Instantiate(currentSpell.secondaryEffects[secondaryEffectCount], transform.position, Quaternion.identity);
            ConnectSpell();
            secondaryEffectCount++;
        }
    }

    //Animation Call
    public virtual void CastSpell()
    {
        Debug.Log("" + this.name + " CastSpell" + currentSpell.name);
        if (spellEffect == null)
        {
            ReadySpell();
        }
        secondaryEffectCount = 0;
        //DisconnectSpell();
        spellEffect = Instantiate(currentSpell.effect, transform.position, Quaternion.identity);
        spellEffect.playerOwner = this;
        ConnectSpell();
        DisconnectSpell();
        SpellAdapter spellAdapter = spellEffect.GetComponent<SpellAdapter>();
        spellAdapter.Adapt(this);
        //spellEffect = null;
        mainCollider.enabled = true;

        currentSpell.OnCast(this);
    }

    public virtual void FinishSpell()
    {
        Debug.Log("" + this.name + " FinishSpell " + currentSpell.name);
        currentSpell.FinishSpell(this);
    }

    public virtual void SetAnimatorSpeed(float speed)
    {
        animator.speed = speed;
    }

    public virtual bool HasAttackSpells()
    {
        //return true;
        foreach (var i in spells)
        {
            if (i.type == SpellType.Attack)
            {
                return true;
            }
        }
        return false;
    }

    private void ConnectSpell()
    {
        CharacterSpellAdapter characterSpellAdapter = GetComponent<CharacterSpellAdapter>();
        characterSpellAdapter.Connect(spellEffect);
    }

    private void DisconnectSpell()
    {
        CharacterSpellAdapter characterSpellAdapter = GetComponent<CharacterSpellAdapter>();
        characterSpellAdapter.Disconnect(spellEffect);
    }

    public void ResetSpellEffect()
    {
        if (spellEffect != null)
        {
            if (spellEffect.gameObject != null)
            {
                spellEffect.DestroyEffect();
            }
            spellEffect = null;
        }
    }

    public bool HasDefenceSpells()
    {
        foreach(var i in spells)
        {
            if (i.type == SpellType.Defence)
            {
                return true;
            }
        }
        return false;
    }


    Transform targetedRagdoll;
    public void Hit(Vector3 hitPosition, GameObject hitSource)
    {
        //animator.SetTrigger("TakeHit");
        ////animator.enabled = false;
        ////mainCollider.enabled = false;
        ////ragdollController.EnableRagdoll();
        //GameObject ragdoll = ragdollController.ragdollObjects[0];
        //float distance = Vector3.Distance(hitPosition, ragdoll.transform.position);
        //foreach (var i in ragdollController.ragdollObjects)
        //{
        //    float d = Vector3.Distance(hitPosition, i.transform.position);
        //    if (d < distance)
        //    {
        //        ragdoll = i;
        //        distance = d;
        //    }
        //}
        //Rigidbody rb = ragdoll.GetComponent<Rigidbody>();
        //targetedRagdoll = rb.transform;
        ////Vector3 dir = (rb.transform.position - hitPosition).normalized;
        ////rb.AddForce(dir * 100, ForceMode.Impulse);
    }

    public void HitEffectApply(GameObject hitEffect)
    {
        if (targetedRagdoll != null)
        {
            hitEffect.transform.parent = targetedRagdoll;
            hitEffect.transform.position = targetedRagdoll.transform.position;
        }
    }

    public void HitSpell(Vector3 hitPosition, GameObject hitSource, AttackSpell attackSpell)
    {
        float damage = attackSpell.damage;

        if (currentSpell!=null&&currentSpell.GetType() == typeof(DefenceSpell))
        {
            ElementalResistance resistance = currentSpell.element.resistancesToElements.Get(attackSpell.element.elementType);
            damage = attackSpell.damage - attackSpell.damage * resistance.resistance;
        }
        else
        {
            overrideController["EmptyHit"] = attackSpell.hitAnim;
            animator.speed = attackSpell.hitAnimSpeed;
            animator.SetTrigger("TakeHit");
        }

        DamageCanvas damageCanvas = PrefabManager.Instance.GetPrefabInstance<DamageCanvas>("DamageCanvas");
        damageCanvas.SetDamageValue(damage);
        damageCanvas.transform.position = transform.position + Vector3.up * 3f;

        if (healthController.ReduceHealth(damage))
        {
            ActivateRagdollDeathEffect(hitPosition);
            Death();
        }
    }

    protected void ResetAnimatorSpeed()
    {
        animator.speed = 1;
    }

    private void ActivateRagdollDeathEffect(Vector3 hitPosition)
    {
        animator.enabled = false;
        mainCollider.enabled = false;
        ragdollController.EnableRagdoll();
        GameObject ragdoll = ragdollController.ragdollObjects[0];
        float distance = Vector3.Distance(hitPosition, ragdoll.transform.position);
        foreach (var i in ragdollController.ragdollObjects)
        {
            float d = Vector3.Distance(hitPosition, i.transform.position);
            if (d < distance)
            {
                ragdoll = i;
                distance = d;
            }
        }
        Rigidbody rb = ragdoll.GetComponent<Rigidbody>();
        targetedRagdoll = rb.transform;
        Vector3 dir = (rb.transform.position - hitPosition).normalized;
        rb.AddForce(dir * 100, ForceMode.Impulse);
    }

    void Death()
    {
        GameManager.Instance.PlayerDied(this);
    }
}
