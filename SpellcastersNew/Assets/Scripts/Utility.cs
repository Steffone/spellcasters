﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public static class Utility
{
    public static T FindComponentInHierarchyBottomUp<T>(Transform obj)
    {
        T comp = obj.GetComponent<T>();
        if (comp != null)
        {
            return comp;
        }
        else if (obj.parent != null)
        {
            return FindComponentInHierarchyBottomUp<T>(obj.parent);
        }
        else
        {
            return default;
        }
    }

    public static IEnumerator DelayCoroutine(Action action,float delay)
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    public static bool InRange(float value1,float value2,float offset)
    {
        if (value1 - offset < value2 && value1 + offset > value2)
        {
            return true;
        }
        return false;
    }
}
