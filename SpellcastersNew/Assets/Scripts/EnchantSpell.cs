﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Untitled enchant Spell", menuName = "Spells/EnchantSpell")]
public class EnchantSpell : Spell
{
    private void Awake()
    {
        type = SpellType.Enchant;
    }
    public override void OnCast(PlayerController playerController)
    {
        throw new System.NotImplementedException();
    }

    public override void OnInitializeSpell(PlayerController playerController)
    {
        throw new System.NotImplementedException();
    }
}
