﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Untitled buff Spell", menuName = "Spells/BuffSpell")]
public class BuffSpell : Spell
{
    private void Awake()
    {
        type = SpellType.Buff;
    }
    public override void OnCast(PlayerController playerController)
    {
        throw new System.NotImplementedException();
    }

    public override void OnInitializeSpell(PlayerController playerController)
    {
        throw new System.NotImplementedException();
    }
}
