﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellDatabase : MonoBehaviour
{
    public static SpellDatabase Instance { get; private set; }

    public List<Spell> spellsTest;

    public Dictionary<string, Spell> spells;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        Populate();
    }

    //TEST
    void Populate()
    {
        spells = new Dictionary<string, Spell>();
        foreach(var i in spellsTest)
        {
            if (!string.IsNullOrEmpty(i.spellCode))
            {
                spells.Add(i.spellCode, i);
            }
            else
            {
                Debug.LogError("Some spell codes are null");
            }

        }
    }

    public Spell GetSpell(string key)
    {
        Spell spell;
        if(spells.TryGetValue(key,out spell))
        {
            return spell;
        }
        return null;
    }

    public bool Exists(string spellCode)
    {
        foreach(var i in spellsTest)
        {
            if (i.spellCode == spellCode)
            {
                return true;
            }
        }
        return false;
    }
}
