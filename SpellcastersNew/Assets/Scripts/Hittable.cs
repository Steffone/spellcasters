﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Hittable : MonoBehaviour, IHittable
{
    public void Hit(Vector3 hitPosition, GameObject hitSource)
    {
        OnHit(hitPosition, hitSource);
    }

    public abstract void OnHit(Vector3 hitPosition, GameObject hitSource);
}
