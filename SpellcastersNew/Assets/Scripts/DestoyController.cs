﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DestoyController : MonoBehaviour
{
    public abstract void DestroyFunction();
}
