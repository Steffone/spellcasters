﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateEffectDestroyController : DestoyController
{
    [SerializeField] SpellEffect destroyEffect;

    public override void DestroyFunction()
    {
        Destroy(gameObject);
        if (destroyEffect != null)
        {
            SpellEffect go = Instantiate(destroyEffect, transform.position, transform.rotation);
            Destroy(gameObject, go.destroyTime / 2);
            Destroy(go.gameObject, go.destroyTime);
        }
    }
}
