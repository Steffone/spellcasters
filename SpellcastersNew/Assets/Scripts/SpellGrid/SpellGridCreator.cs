﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(SpellGridCreator))]
public class SpellGridCreatorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Create"))
        {
            SpellGridCreator sgc = (SpellGridCreator)target;
            sgc.Clear();
            sgc.Create();
        }
    }
}
#endif

public class SpellGridCreator : MonoBehaviour
{
    [SerializeField] int rows;
    [SerializeField] int columns;
    [SerializeField] SpellGridPoint pointPrefab;

    [SerializeField] int fieldWidth;
    [SerializeField] int fieldHeight;
    [SerializeField] int spacing;

    [SerializeField] GridLayoutGroup gridLayoutGroup;

    public void Clear()
    {
        List<GameObject> toDestroy = new List<GameObject>();
        foreach(Transform i in gridLayoutGroup.transform)
        {
            toDestroy.Add(i.gameObject);
        }
        foreach(var i in toDestroy)
        {
            DestroyImmediate(i.gameObject);
        }
    }

    public void Create()
    {
        gridLayoutGroup.cellSize = new Vector2(fieldWidth, fieldHeight);
        gridLayoutGroup.spacing = new Vector2(spacing, spacing);
        gridLayoutGroup.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
        gridLayoutGroup.constraintCount = columns;
        int count = 1;
        for(int i = 0; i < rows; i++)
        {
            for(int j = 0; j < columns; j++)
            {
                SpellGridPoint point = Instantiate(pointPrefab,gridLayoutGroup.transform);
                point.id = count;
                point.pointIdText.text = "" + count;
                count++;
            }
        }
    }
}
