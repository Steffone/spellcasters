﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpellGridController : MonoBehaviourSingleton<SpellGridController>
{

    [SerializeField] UILineRenderer lineRenderer;
    [SerializeField] GameObject _uiEffectsObject;
    [SerializeField] SuccessfullSpellParticle _successfullEffectPrefab;
    [SerializeField] GameObject _unsuccessfullEffectPrefab;

    Action onComplete;
    [SerializeField] Camera _Cam;
    private PlayerController _owner;
    private List<GameObject> _instantiatedEffects;
    private bool _initialized;

    private void OnEnable()
    {
        _uiEffectsObject.SetActive(true);
    }

    private void OnDisable()
    {
        _uiEffectsObject.SetActive(false);
    }

    public List<SpellGridPoint> spellGridPoints { get; private set; }
    public bool mouseDownFlag { get; private set; }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        if (!_initialized)
        {
            _initialized = true;
            _instantiatedEffects = new List<GameObject>();
            spellGridPoints = new List<SpellGridPoint>();
            //_Cam = Camera.main;
        }
    }

    private void Update()
    {
        if(mouseDownFlag)
        {
            SetLastPointToMousePosition();
            if (Input.GetMouseButtonUp(0))
            {
                CreateSpell();
                Reset();
            }
        }
    }

    private void SetLastPointToMousePosition()
    {
        Vector3 worldPos = GetInputPos();
        if (lineRenderer.Points.Count < 2)
        {
            lineRenderer.Points.Clear();
            foreach(var i in spellGridPoints)
            {
                lineRenderer.Points.Add(i.transform.position);
            }

            lineRenderer.Points.Add(worldPos);
        }
        lineRenderer.Points[lineRenderer.Points.Count - 1] = worldPos;

    }

    private void Reset()
    {
        string spellCode = "";
        foreach (var i in spellGridPoints)
        {
            spellCode += "|" + i.id;
            i.Reset();
        }
        spellGridPoints.Clear();
        lineRenderer.Points.Clear();
        mouseDownFlag = false;
    }

    private string CreateSpellCode()
    {
        string spellCode = "";

        foreach (var i in spellGridPoints)
        {
            spellCode += "|" + i.id;
        }

        return spellCode;
    }

    private void CreateSpell()
    {
        string spellCode = CreateSpellCode();
        Spell spell = SpellDatabase.Instance.GetSpell(spellCode);
        if (spell != null)
        {
            SuccessfullSpellParticle effect = Instantiate(_successfullEffectPrefab, _uiEffectsObject.transform);
            effect.transform.localPosition = Vector3.zero;
            effect.SetSprite(spell.icon);
            _instantiatedEffects.Add(effect.gameObject);
            _owner.AddSpell(spell);
        }
        else
        {
            GameObject effect = Instantiate(_unsuccessfullEffectPrefab, _uiEffectsObject.transform);
            effect.transform.localPosition = Vector3.zero;
            _instantiatedEffects.Add(effect);
        }
    }

    public void AddPoint(SpellGridPoint spellGridPoint)
    {
        if (lineRenderer.Points.Count == 0)
        {
            lineRenderer.Points.Add(GetInputPos());
        }
        lineRenderer.Points[lineRenderer.Points.Count - 1] = spellGridPoint.transform.position;
        lineRenderer.Points.Add(GetInputPos());
        //lineRenderer.SetPosition(lineRenderer.positionCount - 1, spellGridPoint.transform.position);
        //lineRenderer.positionCount++;
        spellGridPoints.Add(spellGridPoint);
        SetLastPointToMousePosition();
    }

    public void Open(PlayerController player)
    {
        Init();
        _owner = player;
        gameObject.SetActive(true);
    }

    public void Close()
    {
        Init();
        Reset();
        foreach(var i in _instantiatedEffects)
        {
            if (i != null)
            {
                Destroy(i);
            }
        }
        _instantiatedEffects.Clear();
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    public void BeginSpell()
    {
        mouseDownFlag = true;
    }

    private Vector3 GetInputPos()
    {
        Vector3 pos = _Cam.ScreenToWorldPoint(Input.mousePosition);
        pos.z = transform.position.z;
        return pos;
    }
}
