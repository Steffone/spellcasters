﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class SpellGridPoint : MonoBehaviour,IPointerEnterHandler,IPointerDownHandler
{
    public TextMeshProUGUI pointIdText;

    Image image;

    [SerializeField] Color addedColor;
    [SerializeField] Color basicColor;
    [SerializeField] GameObject runesImage;

    public int id;

    bool added;

    void Start()
    {
        image = GetComponent<Image>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Activate();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Activate(true);
    }

    public void Activate(bool mouseDown=false)
    {
        if (!added && (SpellGridController.Instance.mouseDownFlag||mouseDown))
        {
            SpellGridController.Instance.BeginSpell();
            SpellGridController.Instance.AddPoint(this);
            added = true;
            runesImage.SetActive(true);
        }
    }

    public void Reset()
    {
        runesImage.SetActive(false);
        added = false;
    }

}
