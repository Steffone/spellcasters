﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterParts
{
    LeftHand,
    RightHand,
    LeftArm,
    RightArm,
    LeftLeg,
    RightLeg,
    LeftFoot,
    RightFoot,
    UpperBody,
    LowerBody,
    Head
}

public class CharacterSpellAdapter : MonoBehaviour
{
    public Transform leftHand;
    public Transform rightHand;
    public Transform leftArm;
    public Transform rightArm;
    public Transform leftLeg;
    public Transform rightLeg;
    public Transform leftFoot;
    public Transform rightFoot;
    public Transform upperBody;
    public Transform lowerBody;
    public Transform head;

    public void Connect(SpellEffect spellConnector)
    {
        foreach(var i in spellConnector.spellConnections)
        {
            ResolveBodyPart(i);
        }
    }

    public void Disconnect(SpellEffect spellConnector)
    {
        foreach(var i in spellConnector.spellConnections)
        {
            i.spellTransform.transform.parent = null;
        }
    }

    private void ResolveBodyPart(SpellConnection connection)
    {
        switch (connection.bodyPart)
        {
            case CharacterParts.Head:
                ConnectToBodyPart(head, connection);
                break;
            case CharacterParts.LeftArm:
                ConnectToBodyPart(leftArm, connection);
                break;
            case CharacterParts.RightArm:
                ConnectToBodyPart(rightArm, connection);
                break;
            case CharacterParts.LeftFoot:
                ConnectToBodyPart(leftFoot, connection);
                break;
            case CharacterParts.RightFoot:
                ConnectToBodyPart(rightFoot, connection);
                break;
            case CharacterParts.LeftHand:
                ConnectToBodyPart(leftHand, connection);
                break;
            case CharacterParts.RightHand:
                ConnectToBodyPart(rightHand, connection);
                break;
            case CharacterParts.LeftLeg:
                ConnectToBodyPart(leftLeg, connection);
                break;
            case CharacterParts.RightLeg:
                ConnectToBodyPart(rightLeg, connection);
                break;
            case CharacterParts.UpperBody:
                ConnectToBodyPart(upperBody, connection);
                break;
            case CharacterParts.LowerBody:
                ConnectToBodyPart(lowerBody, connection);
                break;
        }
    }

    private void ConnectToBodyPart(Transform bodyPart, SpellConnection connection)
    {
        connection.spellTransform.parent = bodyPart;
        connection.spellTransform.localPosition = connection.localPosition;
        connection.spellTransform.localRotation = Quaternion.Euler(connection.localRotation);
    }
}

[System.Serializable]
public struct SpellConnection
{
    public CharacterParts bodyPart;
    public Transform spellTransform;
    public Vector3 localPosition;
    public Vector3 localRotation;
}
