﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CloseRangeController : MonoBehaviour
{
    public MeleeAttackSpell meleeAttackSpell;

    public abstract void Initialize(PlayerController player);
    public abstract void MoveTo();
    public abstract void MoveBack();
}
