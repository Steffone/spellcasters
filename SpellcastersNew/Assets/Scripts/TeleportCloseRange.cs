﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportCloseRange : CloseRangeController
{
    PlayerController owner;
    public ParticleSystem speedLineParticleEffect;
    public Material dissolveMaterial;

    public float speed;
    public float loopDelay;

    public Transform targetMove;
    Vector3 rootPos;

    List<Renderer> renderers;

    [SerializeField] AnimationClip dashClip;
    [SerializeField] AnimationClip stopClip;

    public override void Initialize(PlayerController player)
    {
        Debug.Log("" + player.name + " Initialize " + this.name);
        owner = player;
        transform.parent = owner.transform;
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one;
        transform.localRotation = Quaternion.identity;
    }

    public override void MoveTo()
    {
        Debug.Log("" + owner.name + " MoveTo " + this.name);
        rootPos = owner.transform.position;
        StartCoroutine(Teleport());
    }

    public override void MoveBack()
    {
        Debug.Log("" + owner.name + " MoveBack " + this.name);
        StartCoroutine(TeleportBack());
    }

    public void FindAllRenderers(Transform root, List<Renderer> renderers)
    {
        Renderer rend = root.GetComponent<Renderer>();
        if (rend != null)
        {
            renderers.Add(rend);
        }
        foreach(Transform child in root)
        {
            FindAllRenderers(child, renderers);
        }
    }

    private IEnumerator TeleportBack()
    {
        owner.WeaponController.DisableWeaponEffects();
        Vector3 rotation = speedLineParticleEffect.transform.localRotation.eulerAngles;
        rotation.y = 0;
        speedLineParticleEffect.transform.localRotation = Quaternion.Euler(rotation);
        speedLineParticleEffect.Play();

        float value = 0;
        while (value < 1)
        {
            value = Mathf.MoveTowards(value, 1, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        owner.transform.position = rootPos;
        speedLineParticleEffect.Play();
        value = 1;
        while (value > 0)
        {
            value = Mathf.MoveTowards(value, 0, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        rotation.y = 180;
        speedLineParticleEffect.transform.localRotation = Quaternion.Euler(rotation);
        owner.WeaponController.EnableWeaponEffects();
        owner.FinishSpell();
        //owner.FinishSpell();
        Destroy(gameObject);
        //transform.position = rootPos;
    }

    private IEnumerator Teleport()
    {
        renderers = new List<Renderer>();
        FindAllRenderers(owner.transform, renderers);

        owner.WeaponController.DisableWeaponEffects();
        speedLineParticleEffect.Play();

        owner.PlayMovementClip(dashClip);
        float value = 0;
        while (value < 1)
        {
            owner.transform.position = Vector3.MoveTowards(transform.position, targetMove.transform.position, Time.deltaTime * speed);

            value = Mathf.MoveTowards(value, 1, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        PlayerController enemy = GameManager.Instance.GetEnemy(owner);
        owner.transform.position = enemy.transform.position + enemy.transform.forward * 2.5f;
        speedLineParticleEffect.Play();
        value = 1;
        bool once = false;
        while (value > 0)
        {
            value = Mathf.MoveTowards(value, 0, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            if (!once&&value < 0.4f)
            {
                owner.PlayMovementClip(stopClip);
                once = true;
            }
            yield return null;
        }
        //owner.PlayMovementClip(stopClip);
        owner.WeaponController.EnableWeaponEffects();
        yield return new WaitForSeconds(stopClip.length);
        owner.PlayMovementClip(meleeAttackSpell.animationClip);
        yield return new WaitForSeconds(meleeAttackSpell.animationClip.length);

        MoveBack();
        //
        //transform.position = rootPos;
    }

}
