﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportAnimationController : MonoBehaviour
{
    public ParticleSystem particleSystem;
    public List<Renderer> renderers;
    public float speed;
    public float loopDelay;

    public Animator anim;

    public Transform targetMove;
    public Vector3 rootPos;

    public AttackBehaviour attackBehaviour;

    public GameObject weaponEffect;

    private void Awake()
    {
        rootPos = transform.position;
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {
    //        StartCoroutine(Teleport());
    //    }
    //}

    public void StartMeleeAttack()
    {
        StartCoroutine(Teleport());
    }

    public void FinishMeleeAttack()
    {
        StartCoroutine(TeleportBack());
    }

    private IEnumerator TeleportBack()
    {
        weaponEffect.SetActive(false);
        Vector3 rotation = particleSystem.transform.localRotation.eulerAngles;
        rotation.y = 0;
        particleSystem.transform.localRotation = Quaternion.Euler(rotation);
        particleSystem.Play();
        float value = 0;
        while (value < 1)
        {
            value = Mathf.MoveTowards(value, 1, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        transform.position = rootPos;
        particleSystem.Play();
        value = 1;
        while (value > 0)
        {
            value = Mathf.MoveTowards(value, 0, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        rotation.y = 180;
        particleSystem.transform.localRotation = Quaternion.Euler(rotation);
        weaponEffect.SetActive(true);
        GetComponent<PlayerController>().FinishSpell();
        //transform.position = rootPos;
    }

    private IEnumerator Teleport()
    {
        weaponEffect.SetActive(false);
        particleSystem.Play();
        anim.SetBool("Dash",true);
        float value = 0;
        while (value < 1)
        {

            transform.position = Vector3.MoveTowards(transform.position, targetMove.transform.position, Time.deltaTime * speed);

            value = Mathf.MoveTowards(value, 1, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        PlayerController enemy = GameManager.Instance.GetEnemy(GetComponent<PlayerController>());
        transform.position = enemy.transform.position + enemy.transform.forward * 2.5f;
        particleSystem.Play();
        value = 1;
        while (value > 0)
        {
            value = Mathf.MoveTowards(value, 0, Time.deltaTime * speed);
            foreach (var i in renderers)
            {
                i.material.SetFloat("Dissolve", value);
            }
            yield return null;
        }
        anim.SetBool("Dash", false);
        weaponEffect.SetActive(true);
        //transform.position = rootPos;
    }

    //Animation Call
    private void ActivateAttackBehaviour()
    {
        attackBehaviour.enabled = true;
    }
    public void DeactivateAttackBehaviour()
    {
        attackBehaviour.enabled = false;
    }
}
