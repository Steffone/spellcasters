﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationDestroyController : DestoyController
{

   [SerializeField] Animator animator;

    [SerializeField] string destroyTrigger;

    public override void DestroyFunction()
    {
        animator.SetTrigger(destroyTrigger);
    }

    void DestroyAnimationEvent()
    {
        Destroy(gameObject);
    }
}
