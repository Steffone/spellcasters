﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellEffect : MonoBehaviour
{
    public List<SpellConnection> spellConnections;

    public float destroyTime;

    public PlayerController playerOwner;

    private void Start()
    {
        if (spellConnections == null)
        {
            spellConnections = new List<SpellConnection>();
        }
        StartCoroutine(DestoyCoroutine());
    }

    IEnumerator DestoyCoroutine()
    {
        yield return new WaitForSeconds(destroyTime);
        DestoyController destoyController = GetComponent<DestoyController>();
        if (destoyController != null)
        {
            destoyController.DestroyFunction();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void DestroyEffect()
    {
        DestoyController destoyController= GetComponent<DestoyController>();
        if (destoyController != null)
        {
            destoyController.DestroyFunction();
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
