﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpellCastMenuController : MonoBehaviourSingleton<SpellCastMenuController>
{
    [SerializeField] SpellCastButton spellButtonPrefab;
    [SerializeField] Transform scrollContent;
    [SerializeField] GameObject defenceScreen;

    [SerializeField] MeleeAttackSpell defaultMeleeAttackSpell;
    public Button meleeAttackButton;

    PlayerController playerOwner;

    private void ClearScrollBar()
    {
        List<GameObject> toDestroy = new List<GameObject>();
        foreach(Transform i in scrollContent)
        {
            toDestroy.Add(i.gameObject);
        }
        foreach(var i in toDestroy)
        {
            Destroy(i.gameObject);
        }
    }

    private void PopulateScrollBar()
    {
        ClearScrollBar();
        foreach(var spell in playerOwner.spells)
        {
            SpellCastButton spellButton = Instantiate(spellButtonPrefab, scrollContent);
            spellButton.SetData(spell,playerOwner);
        }
    }

    private void PopulateScrollBar(SpellType spellType,bool clear=false)
    {
        if(clear)
            ClearScrollBar();

        foreach (var spell in playerOwner.spells)
        {
            if (spell.type == spellType)
            {
                SpellCastButton spellButton = Instantiate(spellButtonPrefab, scrollContent);
                spellButton.SetData(spell, playerOwner);
            }
        }
    }

    public void Open(PlayerController player)
    {
        meleeAttackButton.gameObject.SetActive(true);
        playerOwner =player;
        PopulateScrollBar(SpellType.Attack,true);
        PopulateScrollBar(SpellType.Buff);
        PopulateScrollBar(SpellType.Enchant);
        gameObject.SetActive(true);
    }

    public void OpenDefence(PlayerController player)
    {
        meleeAttackButton.gameObject.SetActive(false);
        playerOwner = player;
        PopulateScrollBar(SpellType.Defence,true);
        gameObject.SetActive(true);
        //defenceScreen.SetActive(true);
    }

    public void Close()
    {
        //defenceScreen.SetActive(false);
        gameObject.SetActive(false);
    }

    public void MeleeAttack()
    {
        Debug.Log("" + playerOwner.name + " Melee_Button " + defaultMeleeAttackSpell.name);
        //defaultMeleeAttackSpell.OnInitializeSpell(playerOwner);
        playerOwner.StartCastSpell(defaultMeleeAttackSpell);
    }
}
