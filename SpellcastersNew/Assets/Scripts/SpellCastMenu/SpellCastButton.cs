﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class SpellCastButton : MonoBehaviour,IPointerClickHandler
{
    Spell spell;
    PlayerController playerOwner;

    [SerializeField] Image icon;

    [SerializeField] TextMeshProUGUI text;

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("" + playerOwner.name + " Spell_Button "+spell.name);
        playerOwner.StartCastSpell(spell);
    }

    public void SetData(Spell spell,PlayerController player)
    {
        playerOwner = player;
        this.spell = spell;
        text.text = spell.name;
        icon.sprite = spell.icon;
    }
}
