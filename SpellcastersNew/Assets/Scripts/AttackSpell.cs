﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Untitled attack Spell", menuName = "Spells/AttackSpells/Basic Ranged Spell")]
public class AttackSpell : Spell
{
    public AnimationClip hitAnim;
    public float hitAnimSpeed;
    public float damage;

    private void Awake()
    {
        type = SpellType.Attack;
    }

    public override void OnStartCasting(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " OnStartCasting " + this.name);
        playerController.SetupSpellAnimation();
    }

    public override void OnCast(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " OnCast " + this.name);
        GameManager.Instance.Stage_CastSpellsAttackEnd(spellCastDuration);
    }

    public override void OnInitializeSpell(PlayerController playerController)
    {
        base.OnInitializeSpell(playerController);
        Debug.Log("" + playerController.name + " OnInitializeSpell_Attack " + this.name);
        playerController.PlayChargeEffect();
        GameManager.Instance.Stage_CastSpells_AttackStart();
    }
    public override void FinishSpell(PlayerController playerController)
    {
        Debug.Log("" + playerController.name + " FinishSpell AttackSpell" + this.name);
        //GameManager.Instance.Stage_CastSpellsAttackEnd(0);
    }
}
