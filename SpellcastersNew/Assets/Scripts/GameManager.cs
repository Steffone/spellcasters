﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[System.Serializable]
public struct GameSettings
{
    public float STAGE_CreateSpells_ActiveTime;
    public float STAGE_CastSpells_AttakChargeTime;
    public float PlayerSwichDelay;
}

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameSettings gameSettings;

    List<PlayerController> players;
    PlayerController currentActivePlayer;
    PlayerController defencePlayer;

    Coroutine _createSpellsTimerCoroutine;
    private bool _isGameOver;

    public int totalNumberOfSpells;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
    }

    private void Start()
    {
        players = FindObjectsOfType<PlayerController>().ToList();
        Stage_CreateSpells_OnStart();
    }

    private PlayerController GetNextPlayer(PlayerController current,bool activate)
    {
        int indexOfPlayer = 0;
        if (current != null)
        {
            indexOfPlayer = players.FindIndex(x => x == current);
        }

        indexOfPlayer++;
        if (indexOfPlayer == players.Count)
        {
            indexOfPlayer = 0;
        }
        if (activate)
        {
            current?.DeactivateIndicator();
            players[indexOfPlayer].Activate();
            currentActivePlayer = players[indexOfPlayer];
        }
        return players[indexOfPlayer];
    }

    private PlayerController GetPlayer(int index, bool activate)
    {
        if (index >= players.Count || index < 0)
        {
            Debug.LogError("Invalid index");
            return null;
        }
        if (activate)
        {
            currentActivePlayer.DeactivateIndicator();
            players[index].Activate();
            currentActivePlayer = players[index];
        }
        return players[index];
    }

    #region Create Spells
    int stage_CreateSpells_CompleteCount = 0;

    private void Stage_CreateSpells_OnStart()
    {
        foreach (var i in players)
        {
            i.ResetSpellEffect();
            i.spells.Clear();
        }
        Stage_CreateSpells();
    }

    private void Stage_CreateSpells()
    {
        if (_isGameOver)
        {
            return;
        }
        GetNextPlayer(currentActivePlayer,true);
        stage_CreateSpells_CompleteCount++;
        _createSpellsTimerCoroutine = PersonalUtility.Delay(CloseSpellGrid, gameSettings.STAGE_CreateSpells_ActiveTime, this);
        currentActivePlayer.OpenSpellGrid();
    }

    private void CloseSpellGrid()
    {
        currentActivePlayer.CloseSpellGrid();
        if (stage_CreateSpells_CompleteCount == players.Count)
        {
            PersonalUtility.Delay(Stage_CreateSpells_Complete, gameSettings.PlayerSwichDelay, this);
        }
        else
        {
            PersonalUtility.Delay(Stage_CreateSpells, gameSettings.PlayerSwichDelay, this);
        }
    }

    private void Stage_CreateSpells_Complete()
    {
        int spellCount = -1;
        foreach(var i in players)
        {
            if (spellCount < i.spells.Count)
            {
                spellCount = i.spells.Count;
            }
        }

        totalNumberOfSpells = spellCount * 2;
        if (spellCount == 0)
        {
            totalNumberOfSpells = 2;
        }

        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].HasAttackSpells())
            {
                GetPlayer(i, true);
                break;
            }
        }
        Stage_CastSpells();
    }
    #endregion

    #region AICallbacks
    public void FinishCreatingSpells()
    {
        if (_createSpellsTimerCoroutine != null)
        {
            StopCoroutine(_createSpellsTimerCoroutine);
            //_createSpellsTimerCoroutine = null;
        }
        CloseSpellGrid();
    }
    #endregion

    #region Cast Spells
    public void Stage_CastSpells()
    {
        if (_isGameOver)
        {
            return;
        }
        if (!Stage_CastSpells_CheckIfComplete())
        {
            Debug.Log("" + currentActivePlayer.name + " Stage_CastSpells");
            currentActivePlayer.OpenSpellCastMenu();
        }
    }

    public void Stage_CastSpells_AttackStart()
    {
        Debug.Log("" + currentActivePlayer.name + "Stage_CastSpells_AttackStart");
        currentActivePlayer.CloseSpellCastMenu();
        defencePlayer = GetNextPlayer(currentActivePlayer, false);
        defencePlayer.OpenSpellCastMenuDefence();
        SlowDownTime();
    }

    public void Stage_CastSpellsAttackEnd(float delay)
    {
        Debug.Log("" + currentActivePlayer.name + " Stage_CastSpellsAttackEnd delay:"+delay);
        StopAllCoroutines();
        PersonalUtility.Delay(Stage_CastSpells, delay, this);
        PersonalUtility.Delay(delegate { GetNextPlayer(currentActivePlayer, true); }, delay - 0.1f, this);
    }

    public void Stage_CastSpells_DefenceStart()
    {
        Debug.Log("" + defencePlayer.name + " Stage_CastSpells_DefenceStart");
        ContinueSlowDown();
        defencePlayer.CloseSpellCastMenu();
    }

    private void Stage_CastSpells_DefenceEnd()
    {
        Debug.Log("" + defencePlayer.name + " Stage_CastSpells_DefenceEnd");
        currentActivePlayer.StartCastingSpell();
        defencePlayer.CloseSpellCastMenu();
    }

    private bool Stage_CastSpells_CheckIfComplete()
    {

        int count = 0;
        if (totalNumberOfSpells <= 0)
        {
            Stage_CastSpells_Complete();
            return true;
        }
        totalNumberOfSpells--;

        foreach (var player in players)
        {
            bool hasAttackSpell = player.HasAttackSpells();
            if (!hasAttackSpell)
            {
                count++;
            }
        }
        if (count == players.Count)
        {
            Stage_CastSpells_Complete();
            return true;
        }
        return false;
    }

    private void Stage_CastSpells_Complete()
    {
        stage_CreateSpells_CompleteCount = 0;
        PersonalUtility.Delay(Stage_CreateSpells_OnStart, 2, this);
    }

    public PlayerController GetEnemy(PlayerController player)
    {
        foreach (var i in players)
        {
            if (i != player)
            {
                return i;
            }
        }
        return null;
    }

    public void SlowDownTime()
    {
        Debug.Log(Time.frameCount+ "" + currentActivePlayer.name + "SlowDownTime");
        StartCoroutine(TimeSlowDown());
    }

    bool slowDownActive;

    IEnumerator TimeSlowDown()
    {
        float time = gameSettings.STAGE_CastSpells_AttakChargeTime;
        slowDownActive = false;
        while (time > 0|| slowDownActive)
        {
            time -= Time.deltaTime;
            if (time > 0 && slowDownActive)
            {
                time = 0;
            }
            yield return null;
        }
        yield return new WaitForSeconds(0.2f);
        Debug.Log(Time.frameCount+"" + currentActivePlayer.name + "SlowDownTimeCoroutineEnd");
        Stage_CastSpells_DefenceEnd();
    }

    public void ContinueSlowDown()
    {
        Debug.Log("" + defencePlayer.name + " ContinueSlowDown");
        slowDownActive = true;
    }

    public void StopSlowDown()
    {
        Debug.Log("" + defencePlayer.name + " StopSlowDown");
        slowDownActive = false;
    }

    public void PlayerDied(PlayerController player)
    {
        PlayerController winningPlayer = null;
        foreach(var i in players)
        {
            if (i.healthController.GetHealth() > 0)
            {
                if (winningPlayer != null)
                {
                    break;
                }
                winningPlayer = i;
                GameOver(winningPlayer);
            }
        }
    }

    public void GameOver(PlayerController winner)
    {
        Debug.Log(winner.name + " wins the game");
        StopAllCoroutines();
        _isGameOver = true;
        //SceneController.Instance.LoadScene(SceneReferences.GAME_SCENE);
    }
    #endregion
}
