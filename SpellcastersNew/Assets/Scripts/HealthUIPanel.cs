﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthUIPanel : MonoBehaviour
{
    [SerializeField] Image fillImage;
    private CharacterStats _characterStats;
    private Transform _ancorPoint;
    private Camera cam;
    public void Bind(CharacterStats characterStats)
    {
        cam = Camera.main;
        //transform.parent = HealthPanelContainer.Instance.transform;
        //transform.localScale = Vector3.one;
        //_ancorPoint = ancorPoint;
        _characterStats = characterStats;
        _characterStats.OnCurrentHealthChange += Handle_OnCurrentHealthChange;
    }

    //private void Update()
    //{
    //    Vector3 position = RectTransformUtility.WorldToScreenPoint(cam, _ancorPoint.position);
    //    transform.position = position;
    //    Vector3 localpos = transform.localPosition;
    //    localpos.z = 0;
    //    transform.localPosition = localpos;
    //}

    private void Handle_OnCurrentHealthChange(float currentHealth, bool isChanged)
    {
        if (isChanged)
        {
            fillImage.fillAmount = currentHealth / _characterStats.MaxHealth;
        }
    }

    private void OnDestroy()
    {
        _characterStats.OnCurrentHealthChange -= Handle_OnCurrentHealthChange;
    }
}
