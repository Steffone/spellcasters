﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBezier : MonoBehaviour, ITarget
{
    [SerializeField] BezierCurve bezierCurve;
    [SerializeField] float speed;
    public event Action OnFinish;

    public void Init()
    {
        StartCoroutine(Follow());
    }

    public void SetTarget(Transform t)
    {
        BezierPoint[] bezierPoints= bezierCurve.GetAnchorPoints();
        bezierPoints[bezierPoints.Length - 1].position = t.position;
    }

    IEnumerator Follow()
    {
        for (float i = 0; i < 1; i = Mathf.MoveTowards(i, 1, Time.deltaTime * speed))
        {
            transform.position = bezierCurve.GetPointAt(i);
            yield return null;
        }
        OnFinish?.Invoke();
    }
}
