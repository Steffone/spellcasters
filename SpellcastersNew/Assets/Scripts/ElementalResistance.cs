﻿using UnityEngine;

[System.Serializable]
public class ElementalResistance
{
    public SpellElement spellElement;
    [Range(0,1)] public float resistance;

    public ElementalResistance(SpellElement spellElement)
    {
        this.spellElement = spellElement;
    }
}

[System.Serializable]
public class Resistances
{
    public ElementalResistance fireResistance = new ElementalResistance(SpellElement.Fire);
    public ElementalResistance waterResistance = new ElementalResistance(SpellElement.Water);
    public ElementalResistance earthResistance = new ElementalResistance(SpellElement.Earth);
    public ElementalResistance windResistance = new ElementalResistance(SpellElement.Wind);
    public ElementalResistance forceResistance = new ElementalResistance(SpellElement.Force);

    [HideInInspector] ElementalResistance[] lookUpArray;

    public Resistances()
    {
        lookUpArray = new ElementalResistance[5] { fireResistance, waterResistance, earthResistance, windResistance,
                forceResistance };
    }

    public ElementalResistance Get(SpellElement spellElement)
    {
        for (int i = 0; i < lookUpArray.Length; i++)
        {
            if (lookUpArray[i].spellElement == spellElement)
            {
                return lookUpArray[i];
            }
        }
        return null;
    }
}
