﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBehaviour : MonoBehaviour
{
    public AttackSpell attackSpell;

    public List<Transform> coliderTransforms;

    public GameObject hitEffectPrefab;

    public float hitRadius;

    public LayerMask mask;

    bool hitDamage = false;

    [SerializeField] List<GameObject> excludedObjects;

    public void Update()
    {
        //TO DO REFACTOR THERE IS A PROBLEM WITH BREAKING AND MULTIPLE COLIDERS
        foreach (var coliderTransform in coliderTransforms)
        {
            Collider[] hitColliders = Physics.OverlapSphere(coliderTransform.position, hitRadius, mask.value, QueryTriggerInteraction.Collide);
            if (hitColliders.Length != 0)
            {
                bool hit = false;
                foreach (var i in hitColliders)
                {
                    if (excludedObjects.Exists(x => x == i.gameObject))
                    {
                        continue;
                    }
                    if (i.transform.root == transform)
                    {
                        continue;
                    }
                    IHittable hittable = Utility.FindComponentInHierarchyBottomUp<IHittable>(i.transform);
                    hittable?.Hit(coliderTransform.position, gameObject);
                    ISpellHit spellHit = Utility.FindComponentInHierarchyBottomUp<ISpellHit>(i.transform);
                    if (spellHit != null && !hitDamage)
                    {
                        spellHit?.HitSpell(coliderTransform.position, gameObject, attackSpell);
                        hitDamage = true;
                    }
                    if (hitEffectPrefab != null)
                    {
                        Vector3 dir = (i.transform.position - coliderTransform.transform.position).normalized;
                        GameObject hitEffect = Instantiate(hitEffectPrefab, coliderTransform.transform.position, Quaternion.identity);
                        hitEffect.transform.forward = dir;
                        IHitEffectApply hitEffectApply = Utility.FindComponentInHierarchyBottomUp<IHitEffectApply>(i.transform);
                        hitEffectApply?.HitEffectApply(hitEffect);
                        Destroy(hitEffect, 5);
                    }
                    hit = true;
                }
                if (hit)
                {
                    this.enabled = false;
                    break;
                }
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        foreach (var coliderTransform in coliderTransforms)
        {
            Gizmos.DrawWireSphere(coliderTransform.position, hitRadius);
        }
    }

    private void OnEnable()
    {
        hitDamage = false;
    }
}

public interface IHitEffectApply
{
    void HitEffectApply(GameObject hitEffect);
}

public interface ISpellHit
{
    void HitSpell(Vector3 hitPosition, GameObject hitSource,AttackSpell attackSpell);
}


