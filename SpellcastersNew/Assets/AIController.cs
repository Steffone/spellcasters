﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : PlayerController
{
    [System.Serializable]
    public class SpellListConfigurationStruct
    {
        public SpellType SpellType;
        [Header("Inclusive")]
        public int MinSpellCount;
        [Header("Exclusive")]
        public int MaxSpellCount;
        public List<Spell> AvailableSpellList;
    }


    [SerializeField] List<SpellListConfigurationStruct> _spellConfigurationList;
    [SerializeField] List<Spell> _castSpells;
    [Range(0, 1)]
    [SerializeField] float _smartPercentage;

    private void GenerateSpells(SpellType spellType)
    {
        SpellListConfigurationStruct spellConfiguration = _spellConfigurationList.Find(x => x.SpellType == spellType);
        int spellsToCast = Random.Range(spellConfiguration.MaxSpellCount, spellConfiguration.MaxSpellCount);
        for(int i = 0; i < spellsToCast; i++)
        {
            int spellIndex = Random.Range(0, spellConfiguration.AvailableSpellList.Count);
            Spell spell = spellConfiguration.AvailableSpellList[spellIndex];
            _castSpells.Add(spell);
        }
    }

    private Spell GetSpellToCast(SpellType spellType)
    {
        List<Spell> spellOfType = new List<Spell>();
        foreach(var i in spells)
        {
            if (i.type == spellType)
            {
                spellOfType.Add(i);
            }
        }
        int selectedIndex = Random.Range(0, spellOfType.Count);
        if (spellOfType.Count > 0)
        {
            return spellOfType[selectedIndex];
        }
        else
        {
            return null;
        }
    }

    public override void OpenSpellGrid()
    {
        GenerateSpells(SpellType.Defence);
        GenerateSpells(SpellType.Attack);
        spells.AddRange(_castSpells);
        GameManager.Instance.FinishCreatingSpells();
    }

    public override void OpenSpellCastMenu()
    {
        ResetAnimatorSpeed();
        ResetSpellEffect();
        if (!HasAttackSpells())
        {
            GameManager.Instance.Stage_CastSpellsAttackEnd(0);
            return;
        }
        Spell spell = GetSpellToCast(SpellType.Attack);
        StartCastSpell(spell);
    }

    public override void OpenSpellCastMenuDefence()
    {
        Spell spell = GetSpellToCast(SpellType.Defence);
        if (spell==null)
        {
            return;
        }
        StartCastSpell(spell);
    }
}
