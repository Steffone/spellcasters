﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoSingleton_RemoteInitalization<T>  : MonoSingleton_Remote where T:MonoSingleton_Remote
{
    public static T Instance { get; private set; }
    void Init(T obj)
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = obj;
    }

    public override void Initialize(MonoSingleton_Remote monoSingleton_Remote)
    {
        Init((T)monoSingleton_Remote);
    }
}

public abstract class MonoSingleton_Remote : MonoBehaviour
{
    public abstract void Initialize(MonoSingleton_Remote monoSingleton_Remote);
}
