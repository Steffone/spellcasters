﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoBox : Popup<InfoBox>
{
    [SerializeField] TextMeshProUGUI infoText;

    public void Show(string msg)
    {
        infoText.text = msg;
        ShowPopup();
    }
}
