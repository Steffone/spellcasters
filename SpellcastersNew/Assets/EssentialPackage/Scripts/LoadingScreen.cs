﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
public class LoadingScreen : MonoBehaviourSingleton<LoadingScreen>
{
    [SerializeField] TextMeshProUGUI loadingText;
    [SerializeField] Image progressImage;

    bool canClose;

    public event Action OnClose;
    public event Action OnOpen;

    public void Show(float minActiveTime = 0)
    {
        canClose = false;
        UpdateProgress(0);
        UpdateText("");
        gameObject.SetActive(true);
        OnOpen?.Invoke();
        StopAllCoroutines();
        StartCoroutine(CloseAfterMinimumActiveTimer(minActiveTime));
    }

    public void Close()
    {
        canClose = true;
    }

    public void UpdateText(string msg)
    {
        loadingText.text = msg;
    }

    public void UpdateProgress(float value)
    {
        value = Mathf.Clamp01(value);
        progressImage.fillAmount = value;
    }

    private void OnCloseScreen()
    {
        OnClose?.Invoke();
        gameObject.SetActive(false);
    }

    IEnumerator CloseAfterMinimumActiveTimer(float time)
    {
        float t = time;
        while (t > 0)
        {
            UpdateProgress(1 - t / time);
            t -= Time.deltaTime;
            yield return null;
        }
        while (!canClose)
        {
            yield return null;
        }
        OnCloseScreen();
    }
}
