﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class InfoPanel : MonoSingleton_RemoteInitalization<InfoPanel>
{
    [SerializeField] TextMeshProUGUI infoTitle;
    [SerializeField] TextMeshProUGUI infoText;

    public void Show(string title, string msg)
    {
        infoTitle.text = title;
        infoText.text = msg;
        gameObject.SetActive(true);
    }

    public void OnClose()
    {
        gameObject.SetActive(false);
    }
}
