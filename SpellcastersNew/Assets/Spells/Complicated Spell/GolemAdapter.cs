﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemAdapter : SpellAdapter
{
    public float offsetStrength;
    public override void Adapt(PlayerController playerOwner)
    {
        PlayerController enemy = GameManager.Instance.GetEnemy(playerOwner);

        transform.right = playerOwner.transform.forward;

        GameObject target = new GameObject();
        target.transform.parent = transform;
        target.transform.position = enemy.transform.position;
        target.transform.forward = enemy.transform.forward;

        target.transform.position+=(playerOwner.transform.position - target.transform.position).normalized * offsetStrength;

        GetComponent<ITarget>().SetTarget(target.transform);
    }
}
