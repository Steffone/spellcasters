﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RunToCloseRange : CloseRangeController
{
    PlayerController owner;
    Vector3 rootPos;
    [SerializeField] float movementSpeed;
    [SerializeField] AnimationClip clip;
    bool run;
    Vector3 forwardVector;
    public override void Initialize(PlayerController player)
    {
        Debug.Log("" + player.name + " Initialize " + this.name);
        owner = player;
        forwardVector = owner.transform.forward;
        rootPos = owner.transform.position;
        transform.parent = player.transform;
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one;
        transform.rotation = Quaternion.identity;
    }

    public override void MoveBack()
    {
        Debug.Log("" + owner.name + " MoveBack " + this.name);
        StartCoroutine(MoveBackCoroutine());
    }

    IEnumerator MoveBackCoroutine()
    {
        Vector3 target = rootPos;
        run = true;
        owner.MovingAnimation(true);
        NavMeshAgent agent = owner.GetComponent<NavMeshAgent>();
        agent.SetDestination(target);
        yield return null;
        while (agent.remainingDistance > 0.01f)
        {
            owner.SetMovementSpeedAnimation(agent.velocity.magnitude);
            yield return null;
        }
        owner.transform.position = rootPos;
        //while (!Utility.InRange(owner.transform.position.x,target.x,0.01f)&&
        //    !Utility.InRange(owner.transform.position.z, target.z, 0.01f))
        //{
        //    owner.transform.position = Vector3.MoveTowards(owner.transform.position, target, Time.deltaTime * movementSpeed);
        //    yield return null;
        //}
        while (!Utility.InRange(owner.transform.forward.x, forwardVector.x, 0.01f) &&
            !Utility.InRange(owner.transform.forward.z, forwardVector.z, 0.01f))
        {
            owner.SetMovementSpeedAnimation(agent.velocity.magnitude);
            owner.transform.forward = Vector3.MoveTowards(owner.transform.forward, forwardVector, Time.deltaTime * movementSpeed);
            yield return null;
        }
        owner.transform.forward = forwardVector;
        owner.SetMovementSpeedAnimation(0);
        owner.MovingAnimation(false);
        owner.FinishSpell();
        Destroy(gameObject);
    }

    public override void MoveTo()
    {
        Debug.Log("" + owner.name + " MoveTo " + this.name);
        StartCoroutine(MoveToCoroutine());
    }


    IEnumerator MoveToCoroutine()
    {
        PlayerController enemy = GameManager.Instance.GetEnemy(owner);
        Vector3 target = enemy.transform.position + enemy.transform.forward * 2.5f;
        owner.MovingAnimation(true);
        NavMeshAgent agent = owner.GetComponent<NavMeshAgent>();
        agent.SetDestination(target);
        yield return null;
        while (agent.remainingDistance > 0.01f)
        {
            owner.SetMovementSpeedAnimation(agent.velocity.magnitude);
            yield return null;
        }
        //while (!Utility.InRange(owner.transform.position.x, target.x, 0.01f) &&
        //           !Utility.InRange(owner.transform.position.z, target.z, 0.01f))
        //{
        //    owner.transform.position = Vector3.MoveTowards(owner.transform.position, target, Time.deltaTime * movementSpeed);
        //    yield return null;
        //}
        owner.transform.position = target;
        owner.SetMovementSpeedAnimation(0);
        owner.MovingAnimation(false);
        yield return new WaitForSeconds(0.5f);
        owner.PlayMovementClip(meleeAttackSpell.animationClip);
        yield return new WaitForSeconds(meleeAttackSpell.animationClip.length);

        MoveBack();
    }
}
