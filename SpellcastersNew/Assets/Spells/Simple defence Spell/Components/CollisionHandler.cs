﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour,IHittable
{
    [SerializeField] GameObject wavePrefab;
    [SerializeField] Transform waveParents;

    public float timeDelay = 0.1f;

    bool execute=true;

    //private void OnTriggerStay(Collider other)
    //{
    //    ExecuteWave(other.transform);
    //}

    void ExecuteWave(Transform other)
    {
        if (execute)
        {
            Vector3 rayDirection = (transform.position - other.position).normalized;
            Ray ray = new Ray(other.position, rayDirection);
            RaycastHit[] hits = Physics.RaycastAll(ray);
            foreach (var hit in hits)
            {
                if (hit.transform.gameObject == gameObject)
                {
                    foreach (Transform i in waveParents)
                    {
                        GameObject w = Instantiate(wavePrefab, i, false);
                        w.transform.localPosition = new Vector3(hit.textureCoord.x, hit.textureCoord.y, 0);
                        w.transform.localRotation = Quaternion.identity;
                    }
                }
            }

            Vector3 direction = (other.transform.position-transform.position).normalized;

            Vector3 source = other.transform.position + direction * 10;

            Ray rayReverse = new Ray(source, rayDirection);
            hits = Physics.RaycastAll(rayReverse);
            foreach (var hit in hits)
            {
                if (hit.transform.gameObject == gameObject)
                {
                    foreach (Transform i in waveParents)
                    {
                        GameObject w = Instantiate(wavePrefab, i, false);
                        w.transform.localPosition = new Vector3(hit.textureCoord.x, hit.textureCoord.y, 0);
                        w.transform.localRotation = Quaternion.identity;
                    }
                }
            }

            execute = false;
            Invoke("EnableExecute", timeDelay);
        }
    }

    void EnableExecute()
    {
        execute = true;
    }

    public void Hit(Vector3 hitPosition, GameObject hitSource)
    {
        ExecuteWave(hitSource.transform);
    }

}
