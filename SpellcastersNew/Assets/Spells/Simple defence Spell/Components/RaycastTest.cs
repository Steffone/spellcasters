﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastTest : MonoBehaviour
{
    [SerializeField] GameObject wave;
    [SerializeField] Transform waveParents;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 10000))
            {
                //Transform obj = hit.transform;
                //Vector3 dir = hit.point - obj.transform.position;
                //obj.right = dir;
                Debug.Log(hit.textureCoord);
                foreach (Transform i in waveParents)
                {
                    GameObject w = Instantiate(wave, i);
                    wave.transform.localPosition = new Vector3(hit.textureCoord.x, hit.textureCoord.y, 0);
                    wave.transform.localRotation = Quaternion.identity;
                }
            }
        }
    }
}
